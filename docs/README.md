---
home: true
heroImage: /img/icon-colored.png
heroText: Mac App Collection
tagline: Find hidden gems to improve your Mac.
actionText: Show me the apps →
actionLink: /apps/
features:
- title: Native First
  details: Native applications with high level of polish that follows Apple's guidelines and rules is promoted.
- title: Free and Open-Source
  details: If an app have the same polish as a premium macOS app, it will be promoted on the list.
- title: Avoiding Web and Hybrid
  details: I try to avoid any kind of software that uses web/hybrid-frameworks that violate the UI/UX.
meta:
- name: description
  content: Find hidden gems to improve your Mac.
footer: MIT Licensed - Made with VuePress and GitLab CI - Created by Philip Andersen
---


# Why this list?

The software eco-system for macOS over the last decade has been in a huge growing state. There's a lot of new software and apps which offers high quality, native and polished experience, or just enhancing the existing macOS experience to suit personal needs.

Due to the requirements and promotional work by Mac App Store, sooner or later it also made it hard to browse for hidden gems and new upcoming software.

I made this list as a personal goto when others ask me for good software recommendations. This site has no commercial purpose other other than pointing users to support the developers and a link to the Mac App Store. If you do want to support this site and the work I put into this to keep finding new apps, you can send a donation of choice to my [PayPal](https://www.paypal.me/renegadevi/) which will be highly appriciated.


## Help me improve the list

This collection is by no mean perfect. If you want me to add a software to the list or change things up, please give me feedback trough the links in the top right coner or suggest a change on the specific category page. You can also create a [GitLab issue](https://gitlab.com/renegadevi/mac-app-collection/-/issues/new).


## The curation of software

### Software that mess with security

I do not promote or include any software that require lowering the security or disable SIP ("rootless"). Apps should improve the experience of MacOS without any kind of "hacks" and so on. Therefore applications like *MacForge* or *cDock* etc. is rejected.

### Paid Software

In the Apple eco-system, there's a lot higher degree of paid/subscription software with a higher expectency to support developers which has made a great well-crafted app that instead of using example a "free web app" that's just a webpage wrapper. Example of a paid application would be *Sketch*, while a free web app like *Figma* would be rejected.

### Free and Open-Source Software

If an app is FOSS (Free and Open-Source Software) but also feature the same quality as a premium Mac app, that will be on the list. Apps of this kind is *VLC* or *Transmission*, in contrast to *GIMP* or *Audacity* that even tho it does the job, the UI/UX polish, OS integration etc. is not on the same level and therefore rejected. Same reason why a closed-source software like *Screenflow* and *Wirecast* is on the list, but not *OBS*.

### Oudated & Legacy software

Software that hasn't been updated but also isn't relevant anymore is rejected. Examples would be apps like *Growl* or *Hyperdock* while others that even tho hasn't been updated in years can be still relevant and useful today, such as *Burn* even tho most of us don't own a DVD-burner anymore.

### Frameworks

As for UI, libraries, frameworks etc, the more native look and feel of a software where it looks like a part of the OS it self is what's promoted. Example of what's preferred is Cocoa, Qt, AppKit, SwiftUI (Catalyst) etc. while rejecting most CEF, Electron, JavaFX, Swing etc. frameworks.

Most web applications wrappers does not comply with the UI guidelines, drains the battery, increase both your internet bandwidth and RAM usage, reimplement existing OS features while many times lacking native OS features and therefore is rejected.

### Privacy Policies

In this day and age it's hard to stay away from software that in one or another way collect personal data on you as it's not always told, or discovered until much later due to the nature of closed-source software. But as a general guideline, apps that's been known to be privacy infringing, do inferious data collecting etc. will mostly be rejected.

### No company-specific software

For any software that's specific to a single platform or company is rejected. Things of that kind would be *Github Desktop* or *Tunnelbear VPN*, while a app like *Sourcetree* made by Atlassian would be on the list.



## Copyright notice

All graphical assets such as symbols and logos is made by Philip Andersen.

"Mac App Collection" is an independent site and has not been authorized, sponsored, or otherwise approved by Apple Inc.

"Mac App Store" and "macOS" is a trademark of Apple Inc., registered in the U.S. and other countries. Mac is a trademark of Apple Inc.
