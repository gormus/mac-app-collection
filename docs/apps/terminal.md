# Terminal

## Emulators
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[iTerm](https://www.iterm2.com/)| Powerful and full-featured Terminal-replacement for Terminal.|[Website](https://www.iterm2.com/)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Alacritty](https://github.com/alacritty/alacritty)| A super fast, cross-platform, GPU-accelerated terminal emulator.|[Github](https://github.com/alacritty/alacritty)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[Cathode](https://apps.apple.com/us/app/cathode/id499233976?mt=12)| Vintage terminal emulator.|[AppStore](https://apps.apple.com/us/app/cathode/id499233976?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Serial](https://www.decisivetactics.com/products/serial/)| Connect to routers, servers, firewalls, industrial control and IoT devices with ease.|[AppStore](https://apps.apple.com/us/app/serial/id877615577?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Miscellaneous
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[ASCIInema](https://asciinema.org/)| Lightweight, purely text-based terminal screen recording and easy to share sessions.|[Website](https://asciinema.org/docs/getting-started)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|

