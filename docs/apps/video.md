# Video

## Converters
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Motion](https://www.apple.com/final-cut-pro/motion/)| Powerful motion graphics tool for cinematic 2D, 3D, and 360° titles, fluid transitions, and realistic effects in real time.|[AppStore](https://apps.apple.com/us/app/motion/id434290957?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Compressor](https://www.apple.com/final-cut-pro/compressor/)| Add power and flexibility to your video conversions. Quickly customize output settings, enhance images, and package your film for sale on the iTunes Store.|[AppStore](https://apps.apple.com/us/app/compressor/id424390742?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Handbrake](https://handbrake.fr/)| Tool for converting video from nearly any format to a selection of modern, widely supported codecs.|[Website](https://handbrake.fr/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|


## Media players
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[IINA](https://iina.io/)| A modern media player for MacOS.|[Website](https://iina.io/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[VLC](http://www.videolan.org/)| Free and open source multimedia player and framework that plays most files and various streaming protocols.|[Website](http://www.videolan.org/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[Elmedia Video Player](https://www.elmedia-video-player.com/)| A can-do-it-all Mac video player with advanced streaming options.|[AppStore](https://apps.apple.com/us/app/elmedia-video-player/id1044549675?mt=12)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Switch](http://www.telestream.net/switch/)| Professional Multiformat video player & encoder with inspection and correction.|[Website](http://www.telestream.net/switch/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[OmniPlayer](http://omniplayer.net/support-omniplayer.html)| Comprehensive media player for almost any video and audio format on macOS with powerful features and modern clean design.|[AppStore](https://apps.apple.com/app/omniplayer-mkv-video-player/id1470926410?mt=12)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Screen recording / Streaming
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[ScreenFlow](http://www.telestream.net/screenflow/)| Easy to use and powerful screen recording software with built-in video editor.|[AppStore](DownloadLink)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Wirecast](http://www.telestream.net/wirecast/)| Professional live video streaming production software.|[Website](http://www.telestream.net/wirecast/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Screenflick](https://www.araelium.com/screenflick-mac-screen-recorder)| Mac screen recording with audio, keystrokes, and mouse clicks.|[Website](https://www.araelium.com/screenflick-mac-screen-recorder)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|

## Video editors
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Final Cut Pro X](https://www.apple.com/final-cut-pro/)| Professional post-production video editor.|[AppStore](https://apps.apple.com/us/app/final-cut-pro/id424389933?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[iMovie](https://www.apple.com/imovie/)| Streamlined easy to use movie editor.|[AppStore](https://apps.apple.com/us/app/imovie/id408981434?mt=12)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Mitti](https://imimot.com/mitti/)| Easy to use and powerful video cue software for macOS|[Website](https://imimot.com/mitti/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Tools
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[MediaInfo](https://mediaarea.net/en/MediaInfo)| MediaInfo is a convenient unified display of the most relevant technical and tag data for video and audio files.|[AppStore](https://apps.apple.com/us/app/mediainfo/id510620098?mt=12)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Pixel Perfect LED](https://reddotlogics.com/product/pixel-perfect-led-single-license/)| A utility to generate pixel map for displays for events.|[Website](https://reddotlogics.com/product/pixel-perfect-led-single-license/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Helium](https://heliumfloats.com/)| Helium is a floating browser window that allows you to watch media while you work.|[AppStore](https://apps.apple.com/us/app/helium/id1054607607?ls=1&mt=12)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|

