# System

## Battery
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Batteries](https://www.fadel.io/batteries)| Track all your devices' batteries from your Mac. |[Website](https://www.fadel.io/batteries)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[coconutBattery](https://www.coconut-flavour.com/coconutbattery/)| Inspect your battery life and get live and current health information, as well in your menu bar.|[Website](https://www.coconut-flavour.com/coconutbattery/)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Juice!](https://www.ettoresoftware.com/mac-apps/juice/)| Keeps an eye on your laptop’s battery charge level and can play a sound when it’s time to plug in or when it’s OK to disconnect from the mains.|[AppStore](https://apps.apple.com/us/app/juice/id1456354072?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Wattagio](https://filincode.com/en/wattagio)| Powerful battery assistant for your Mac. It gives you information about current battery health, including battery life, battery consumption, and battery cycle count.|[Website](https://filincode.com/en/wattagio)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[FruitJuice](https://fruitjuiceapp.com/)| FruitJuice will let you know how long to stay unplugged each day to keep your battery healthy.|[AppStore](https://apps.apple.com/us/app/fruitjuice-battery-health/id671736912)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Bluetooth
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Juice](https://deadbeef.me/Juice/)| The missing Bluetooth manager for macO (Note: No longer maintained, tested and works in Catalina).|[Website](https://deadbeef.me/Juice/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[AirBuddy](https://gumroad.com/l/airbuddy)| AirBuddy brings the same AirPods experience you have on iOS to the Mac. |[Website](https://gumroad.com/l/airbuddy)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Desktop
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Free My Desktop](http://www.valentinourbano.com/Free-my-desktop-mac-app.html)| Automatically hides all the icons from your desktop, the dock and the menubar.|[Website](http://www.valentinourbano.com/Free-my-desktop-mac-app.html)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[HiddenMe](https://appersian.net/)| Hide your desktop icons with a single click|[AppStore](https://apps.apple.com/us/app/hiddenme/id467040476)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Display
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[SwitchResX](https://www.madrau.com/)| Get full control of your displays|[Website](https://www.madrau.com/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[f.lux](https://justgetflux.com/)| It makes the color of your computer's display adapt to the time of day, warm at night and like sunlight during the day. |[Website](https://justgetflux.com/)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[MonitorControl](https://github.com/MonitorControl/MonitorControl)| Control your external monitor brightness & volume on your Mac |[Github](https://github.com/MonitorControl/MonitorControl)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[GammaControl](https://michelf.ca/projects/gamma-control/)| Per-screen color adjustments.|[AppStore](https://apps.apple.com/us/app/gamma-control/id1250521285?l=en)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[QuickRes](https://thnkdev.com/QuickRes/)| Quick and simple resolution changer for MacOS as a menubar dropdown.|[Website](https://thnkdev.com/QuickRes/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Displays](https://www.jibapps.com/apps/displays/)| Manage your monitors and more|[Website](https://www.jibapps.com/apps/displays/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## File system / Drivers
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[FUSE for MacOS](https://osxfuse.github.io/)| FUSE extends macOS by adding support for user space file systems|[Website](https://osxfuse.github.io/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[Mounty for NTFS](https://mounty.app/)| A tiny tool to re-mount write-protected NTFS volumes under macOS in read-write mode.|[Website](https://mounty.app/)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Paragon NTFS for Mac](https://www.paragon-drivers.com/en/ntfsmac/)| Seamless first-class support for NTFS. Write, edit, copy, move and delete files easy on your Mac.|[Website](https://www.paragon-drivers.com/en/ntfsmac/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Paragon extFS for Mac](https://www.paragon-drivers.com/en/extfsmac/)| Read or write files from HDD, SSD or flash drives formatted under Linux ext2, ext3, and ext4 file systems. Just connect the disk and access the files.|[Website](https://www.paragon-drivers.com/en/extfsmac/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Blue Harvest](https://apps.apple.com/us/app/blueharvest/id739483376?mt=12)| BlueHarvest keeps your devices free of macOS and Windows metadata such as ._ files, .DS_Store files, .Trashes, Thumbs.db, Desktop.ini and $RECYCLE.BIN etc.|[AppStore](https://apps.apple.com/us/app/blueharvest/id739483376?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Microsoft NTFS by Tuxera](https://www.tuxera.com/products/tuxera-ntfs-for-mac/)| Get reliable read-write compatibility for all NTFS-formatted USB drives|[Website](https://www.tuxera.com/products/tuxera-ntfs-for-mac/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[AutoMounter](https://www.pixeleyes.co.nz/automounter/)| AutoMounter ensures that your shares are always mounted when you need them.|[AppStore](https://apps.apple.com/us/app/automounter/id1160435653?ls=1&mt=12)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Keyboard
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[KeyCastr](https://github.com/keycastr/keycastr)| An open-source keystroke visualizer |[Github](https://github.com/keycastr/keycastr)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[Karabiner-Elements](https://karabiner-elements.pqrs.org/)| A powerful and stable keyboard customizer for macOS.|[Website](https://karabiner-elements.pqrs.org/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[Visualize](https://www.lisburntech.com/apps/visualize/)| Visualize allows you to highlight your key presses and mouse clicks right on your desktop!|[AppStore](https://apps.apple.com/app/id978980906?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Management
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[AppCleaner](https://freemacsoft.net/appcleaner/)| A small application which allows you to thoroughly uninstall unwanted apps.|[Website](https://freemacsoft.net/appcleaner/)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[DaisyDisk](https://daisydiskapp.com/)| Analyze disk usage and clear up space|[AppStore](https://apps.apple.com/us/app/daisydisk/id411643860?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[LaunchControl](https://www.soma-zone.com/LaunchControl/)| LaunchControl is a fully-featured launchd GUI allowing you to create, manage and debug system- and user services on your Mac.|[Website](https://www.soma-zone.com/LaunchControl/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Lingon](https://www.peterborgapps.com/lingon/)| An easy to use yet powerful app to run things automatically|[Website](https://www.peterborgapps.com/lingon/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Disk Inventory X](http://www.derlien.com)| Disk usage utility for Mac OS X. It shows the sizes of files and folders in a special graphical way called "treemaps".|[Website](http://www.derlien.com)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[OmniDiskSweeper](https://www.omnigroup.com/more)| This shows you the files on your drive, largest to smallest, and lets you quickly Trash or open them.|[Website](https://www.omnigroup.com/more)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Darksight](https://www.imagetasks.com/disksight-disk-cleaner/)| Visualize your disk, so you can easily find and remove large folders and files that might be not needed anymore.|[AppStore](https://apps.apple.com/us/app/disksight/id1485505524?ls=1&mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Launch Services](https://apps.tempel.org/LaunchServices/index.php)| Uses the macOS Launch Services API to perform lookups of bundle IDs and file extensions.|[Website](https://apps.tempel.org/LaunchServices/index.php#download)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Mouse / Trackpad
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[BetterTouchTool](https://folivora.ai/)| BetterTouchTool is a great, feature packed app that allows you to customize various input devices on your Mac.|[Website](https://folivora.ai/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Mos](https://mos.caldis.me/)| A lightweight tool used to smooth scrolling and set scroll direction independently for your mouse on MacOS.|[Website](https://mos.caldis.me/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[SmoothScroll](https://www.smoothscroll.net/mac/)| Give your mouse wheel buttery smooth scrolling.|[Website](https://www.smoothscroll.net/mac/)|![Subscription](symbols/subscription.svg "Subscription")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Swish](https://highlyopinionated.co/swish/)| Control windows and applications right from your trackpad with intuitive two-finger swipe, pinch, tap and hold gestures.|[Website](https://highlyopinionated.co/swish/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Mouseposé](https://boinx.com/mousepose/)| Add spotlight to your mouse cursor.|[Website](https://boinx.com/mousepose/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Middle](https://middleclick.app)| Add middle click to your Mac|[Website](https://middleclick.app/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[MultiTouch](https://multitouch.app/)| Easily add more gestures to macOS.|[Website](https://multitouch.app/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[SteerMouse](https://plentycom.jp/en/steermouse/index.html)| A utility that lets you freely customize buttons, wheels and cursor speed. Both USB and Bluetooth mice are supported.|[Website](https://plentycom.jp/en/steermouse/index.html)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[CursorSense](https://plentycom.jp/en/cursorsense/index.html)| An application that adjusts cursor acceleration and sensitivity. It can move the cursor to a specified destination, such as OK and Cancel button automatically.|[Website](https://plentycom.jp/en/cursorsense/index.html)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Notification Center
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[muzzle](https://muzzleapp.com/)| A simple app to silence embarrasing notifications while screensharing.|[Website](https://muzzleapp.com/)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Package management
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Homebrew](https://brew.sh/)| Most popular and easy to use package manager for MacOS.|[Website](https://brew.sh/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[MacPorts](https://www.macports.org/)| Ports for compiling, installing, and upgrading either command-line X11 or Aqua based open-source software.|[Website](https://www.macports.org/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[MAS](https://github.com/mas-cli/mas)| Mac App Store command line interface|[Github](https://github.com/mas-cli/mas)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[Cakebrew](https://www.cakebrew.com/)| The Mac App (GUI) for Homebrew.|[Website](https://www.cakebrew.com/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[Latest](https://max.codes/latest/)| App for macOS that checks if all your apps are up to date.|[Website](https://max.codes/latest/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|


## Sleep
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Lungo](https://sindresorhus.com/lungo)| Prevent your Mac from going to sleep|[AppStore](https://apps.apple.com/us/app/lungo/id1263070803?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[KeepingYouAwake](https://github.com/newmarcel/KeepingYouAwake)| Prevents your Mac from going to sleep.|[Github](https://github.com/newmarcel/KeepingYouAwake)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[Amphetamine](https://apps.apple.com/de/app/amphetamine/id937984704?mt=12)| Amphetamine can keep your Mac, and optionally its display(s), awake through a super simple on/off switch, or automatically through easy-to-configure Triggers.|[AppStore](https://apps.apple.com/de/app/amphetamine/id937984704?mt=12)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[App Tamer](https://www.stclairsoft.com/AppTamer/)| Detect applications and take control over apps that is resource hogs. App Tamer will automatically slow down or pause your applications whenever you're not using them, greatly reducing their CPU use.|[Website](https://www.stclairsoft.com/AppTamer/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## System information
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Intel MacCPUID](https://software.intel.com/en-us/download/download-maccpuid)| MacCPUID is a tool used for displaying information collected from the microprocessor via the CPUID instruction.|[Website](https://software.intel.com/en-us/download/download-maccpuid)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Intel Power Gadget](https://software.intel.com/en-us/articles/intel-power-gadget)| Software-based power usage monitoring tool for Intel CPUs|[Website](https://software.intel.com/en-us/articles/intel-power-gadget)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Sensei](https://sensei.app/)| Monitor, tweak and optimize your Mac with a easy to see dashboard.|[Website](https://sensei.app/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[iStat Menus](https://bjango.com/mac/istatmenus/)| An advanced system monitor for your menubar.|[AppStore](https://apps.apple.com/us/app/istat-menus/id1319778037?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[EtreCheck](http://www.etrecheck.com/)| Produce a comprehensive diagnostic report on the current state of your computer. This report includes details about hardware, installed software, and performance.|[AppStore](https://apps.apple.com/us/app/etrecheck/id1423715984?mt=12)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Sloth](https://github.com/sveinbjornt/Sloth)| Show all open files, directories, sockets, pipes and devices in use by all running processes. GUI for lsof.|[Website](https://github.com/sveinbjornt/Sloth)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[iStatistica](https://www.imagetasks.com/system-battery-network-monitor-widget/)| Advanced system monitor for macOS. It includes notification center widget and status bar menu|[AppStore](https://apps.apple.com/us/app/istatistica/id1025822138?ls=1&mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[FreeSpace](http://freespaceosx.0fees.net/FreeSpace_-_Shows_Available_Space_on_All_of_Your_Drives_in_Single_Menu.html?i=1)| Show free space of your drives in a menubar app, including TimeMachine and hidden local TimeMachine snapshots.|[AppStore](https://apps.apple.com/us/app/freespace/id457520846?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Stats](https://github.com/exelban/stats)| Simple macOS system monitor in your menu bar to show cpu, ram, network, battery and sensors.|[Github](https://github.com/exelban/stats)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|


## Tiling window managers
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Yabai](https://github.com/koekeishiya/yabai)| A tiling window manager for macOS based on binary space partitioning.|[Github](https://github.com/koekeishiya/yabai)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[Amethyst](https://ianyh.com/amethyst/)| Tiling window manager for macOS along the lines of xmonad.|[Website](https://ianyh.com/amethyst/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|


## System Tweaks
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Fanny](https://www.fannywidget.com/)| Notification Center Widget and Menu Bar application to monitor your Macs fans.|[Website](https://www.fannywidget.com/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[Monolingual](https://github.com/IngmarStein/Monolingual)| A tool for removing unneeded language localization files for macOS|[Github](https://github.com/IngmarStein/Monolingual)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[New File Menu](https://langui.net/new-file-menu/)| New File Menu allows you to create new files quickly via the Finder context menu. |[AppStore](https://apps.apple.com/us/app/new-file-menu/id1064959555?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Context Menu](https://langui.net/context-menu/)| Context Menu is a configurable contextual menu extension for Finder.|[AppStore](https://apps.apple.com/app/context-menu/id1236813619?l=en&mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[PrefEdit](https://www.bresink.com/osx/PrefEdit.html)| PrefEdit is an application to manage nearly all aspects of the preference system contained in every macOS installation.|[Website](https://www.bresink.com/osx/PrefEdit.html)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Prefs Editor](https://apps.tempel.org/PrefsEditor/)| A GUI for the 'defaults' command. a.k.a. the Registry Editor for macOS.|[Website](https://apps.tempel.org/PrefsEditor/)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Easy New File](https://liufsd.github.io/2018/04/29/Easy-New-File/)| A finder extension which adds some functions in right click menu in the Finder|[AppStore](https://apps.apple.com/app/easy-new-file/id1162194131?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Window layout / Snapping
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Rectangle](https://rectangleapp.com/)| Rectangle is FOSS window management app based on Spectacle, written in Swift. |[Website](https://rectangleapp.com/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[Magnet](https://magnet.crowdcafe.com/)| Magnet declutters your screen by snapping windows into organized tiles.|[AppStore](https://apps.apple.com/app/id441258766?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Moom](https://manytricks.com/moom/)| Moom makes window management as easy as clicking a mouse button—or using a keyboard shortcut.|[AppStore](https://apps.apple.com/us/app/moom/id419330170?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Mosaic](https://www.lightpillar.com/mosaic.html)| Effortlessly resize and re-position windows on your Mac.|[Website](https://www.lightpillar.com/mosaic.html)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
 
 
## Window management
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Alt-Tab](https://github.com/lwouis/alt-tab-macos)| Brings the power of Windows' "alt-tab" window switcher to macOS.|[Github](https://github.com/lwouis/alt-tab-macos#alt-tab-macos)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[WindowSwitcher](https://noteifyapp.com/windowswitcher/)| Powerful Mac Desktop Window Switcher and Window Manager for macOS|[Website](https://noteifyapp.com/windowswitcher/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[MaxSnap](https://noteifyapp.com/maxsnap-mac-window-manager/)| Powerful Window Manager for Mac, Alternative to SizeUp, Divvy and Moom.|[Website](https://noteifyapp.com/maxsnap-mac-window-manager/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[ScreenFocus](https://www.apptorium.com/screenfocus)| Dimms any number of screens, so that you can focus only on what you are actually working on. |[AppStore](https://apps.apple.com/us/app/screenfocus/id1337028713?ct=apt&ls=1&mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[HazeOver](https://hazeover.com/)| Distraction Dimmer for Mac.|[Website](https://hazeover.com/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[CommandQ](https://commandqapp.com/)| Never accidentally quit an app or mistakenly close a window again.|[Website](https://commandqapp.com/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Hocus Focus](http://hocusfoc.us/)| A Mac menu bar utility that hides your inactive windows.|[Website](http://hocusfoc.us/)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Window Focus](https://fiplab.com/apps/window-focus-for-mac)| Highlight your active window and dim the rest|[Website](https://fiplab.com/apps/window-focus-for-mac)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Contexts](https://contexts.co/)| Switch between application windows effortlessly.|[Website](https://contexts.co/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
