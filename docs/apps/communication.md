# Communication

## Chat clients

| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Adium](https://adium.im/)| Adium is a free instant messaging application for Mac OS X that can connect to AIM, XMPP (Jabber), ICQ, IRC and more.|[Website](https://adium.im/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[Seaglass](https://github.com/neilalexander/seaglass)| Seaglass is a native macOS client for Matrix. It is written in Swift and uses the Cocoa user interface framework.|[Github](https://github.com/neilalexander/seaglass#seaglass)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|


## Social network

| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[TweetBot](https://tapbots.com/tweetbot/mac/)| Full-featured Twitter client for the Mac with multiple-column support and much more.|[AppStore](https://apps.apple.com/us/app/tweetbot-3-for-twitter/id1384080005?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## IRC

| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Textual](https://www.codeux.com/textual/)| Textual is the most popular GUI application for IRC on macOS.|[AppStore](https://apps.apple.com/us/app/textual-7/id1262957439?mt=12)|![Paid](symbols/paid.svg "Paid")|![Open-Source](symbols/open.svg "Open-Source")|


## Messages

| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Chatology](https://flexibits.com/chatology)| Search and archive trough your message history (iMessage/GoogleTalk/Jabber).|[Website](https://flexibits.com/chatology)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
