# Utilities

## Archiving
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[The Unarchiver](https://theunarchiver.com/)| Open any archive in seconds.|[AppStore](https://itunes.apple.com/us/app/the-unarchiver/id425424353?mt=12)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[BetterZip](https://macitbetter.com/)| Advanced archiver that does everything you need.|[Website](https://macitbetter.com/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Keka](https://www.keka.io/)| Keka is a full featured file archiver, as easy as it can be.|[AppStore](https://apps.apple.com/app/keka/id470158793)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Archiver](https://archiverapp.com/)| Easy to use archiver.|[Website](https://archiverapp.com/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Dark mode / Light mode 
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Shifty](https://shifty.natethompson.io/en/)| A macOS menu bar app that gives you more control over Night Shift.|[Website](https://shifty.natethompson.io/en/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[Gray](https://github.com/zenangst/Gray)| Tailor your Dark/Light mode experience per app.|[Github](https://github.com/zenangst/Gray)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[NightOwl](https://nightowl.kramser.xyz/)| Easily toggle MacOS dark mode in the menu bar.|[Website](https://nightowl.kramser.xyz/)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Nocturnal](https://github.com/HarshilShah/Nocturnal)| A simple macOS app to toggle dark mode with one click.|[Github](https://github.com/HarshilShah/Nocturnal)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|


## Menu bar
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Bartender](https://www.macbartender.com/)| Organize your menu bar icons with a second menu.|[Website](https://www.macbartender.com/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Dozer](https://github.com/Mortennn/Dozer)| Hide status bar icons. |[Github](https://github.com/Mortennn/Dozer)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[Hidden Bar](https://github.com/dwarvesf/hidden)| An ultra-light MacOS utility that helps hide menu bar icons.|[AppStore](https://apps.apple.com/app/hidden-bar/id1452453066)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[Vanilla](https://matthewpalmer.net/vanilla/)| Hide menu bar icons on your Mac.|[Website](https://matthewpalmer.net/vanilla/)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[DarkNight](https://hobbyistsoftware.com/darknight)| Automatically switch between light and dark wallpapers based on sunrise and sunset, or on your own schedule.|[AppStore](https://itunes.apple.com/us/app/dark-night/id1451526460)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[BitBar](https://getbitbar.com/)| Put anything in your menu bar. |[Github](https://github.com/matryer/bitbar#-bitbar)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|


## Touch bar
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[MTMB - My TouchBar My Rules"](https://mtmr.app/)| The TouchBar Customization App for your MacBook Pro.|[Website](https://mtmr.app/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[Haptic Touch Bar App](https://www.haptictouchbar.com/)| Haptic Touch Bar provides actual feedback when pressing buttons on your Touch Bar.|[Website](https://www.haptictouchbar.com/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Touch Bar Simulator](https://sindresorhus.com/touch-bar-simulator/)| The macOS Touch Bar as a standalone app.|[Website](https://sindresorhus.com/touch-bar-simulator/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[TouchSwitcher](https://hazeover.com/touchswitcher.html)| Use Touch Bar to Switch Apps.|[Website](https://hazeover.com/touchswitcher.html)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Muse](https://xzzz9097.github.io/)| An open-source Spotify, iTunes and Vox controller with TouchBar support.|[Github](https://github.com/xzzz9097/Muse)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|


## Virtualization
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Parallels Desktop](https://www.parallels.com/eu/products/desktop/)| Best way to emulate Windows on your Mac with a seamless experience.|[Website](https://www.parallels.com/eu/products/desktop/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[VMware Fusion](https://www.vmware.com/products/fusion.html)| Fusion is simple and powerful VM to run operating systems side by side with Mac applications, without rebooting.|[Website](https://www.vmware.com/products/fusion.html)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Other
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Endurence](http://enduranceapp.com/)| Add a power-saving mode for our Mac and boost your battery.|[Website](http://enduranceapp.com/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Burn](https://burn-osx.sourceforge.io/)| Simple but advanced CD and DVD curner for macOS.|[Website](https://burn-osx.sourceforge.io/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[What's Open](https://apps.tempel.org/WhatsOpen/index.php)| A simple utility for MacOSX which lists the open files on a system and allows the user to kill the process that has them open.|[Website](https://apps.tempel.org/WhatsOpen/index.php)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|

