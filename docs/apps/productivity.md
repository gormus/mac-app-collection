# Productivity

## Accounting / Finance / Administration
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Inventoria Plus](https://www.nchsoftware.com/inventory/index.html)| Manage and monitor your inventory with Inventoria to help streamline your operations and boost profits.|[AppStore](https://apps.apple.com/us/app/inventoria-plus/id976205589?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Grandtotal](https://www.mediaatelier.com/GrandTotal6/)| Create invoices and estimates on your Mac|[Website](https://www.mediaatelier.com/GrandTotal6/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[WarRoom](https://warroom.calvatronics.com/)| Lightweight but powerful document review tool for litigation support. Designed for attorneys and litigation support professionals.|[AppStore](https://apps.apple.com/us/app/warroom/id485185814?mt=12)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Calendar
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Calendarique](https://www.imagetasks.com/calendar-widget-for-osx/)| Calendar for Desktop, Notification Center and Menu Bar.|[AppStore](https://apps.apple.com/us/app/calendarique/id1040634920?ls=1&mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[CornerCal](https://github.com/ekreutz/CornerCal)| A simple, clean calendar and clock app for macOS.|[Github](https://github.com/ekreutz/CornerCal)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[Dato](https://sindresorhus.com/dato)| Better menu bar date and time replacement|[AppStore](https://apps.apple.com/us/app/dato/id1470584107?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Fantastical](https://flexibits.com/fantastical)| Advanced calender app that's works cross all devices.|[Website](https://flexibits.com/fantastical)|![Subscription](symbols/subscription.svg "Subscription")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Itsycal](https://www.mowglii.com/itsycal/)| Itsycal is a tiny menu bar calendar with tasks.|[Website](https://www.mowglii.com/itsycal/)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[BusyCal](https://www.busymac.com/busycal/index.html)| Powerful calender app replacement based on the MacOS calendar design|[AppStore](https://apps.apple.com/us/app/busycal/id1173663647?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[InstaCal](http://instacalapp.com/)| InstaCal is an affordable and powerful Mac calendar app that puts all your events and reminders in your Mac menu bar.|[AppStore](https://apps.apple.com/us/app/instacal-menu-bar-calendar/id1247292524?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[MonthlyCal](https://doublerew.net/os-x/monthlycal/)| A colorful calendar widget|[AppStore](https://apps.apple.com/us/app/monthlycal/id935250717?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Blotter](https://www.wireload.net/products/blotter/)| Present your Calendar events and reminders right on your desktop.|[AppStore](https://apps.apple.com/us/app/blotter/id406580224?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Calculators
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Calcbot](https://tapbots.com/calcbot/mac/)| An Intelligent Calculator and Unit Converter.|[AppStore](https://apps.apple.com/us/app/calcbot-the-smart-calculator/id931657367)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[PocketCAS](https://pocketcas.com/)| Very advanced mathematics application that helps you with any kind of math problem from elementary school to calculus, algebra and statistics.|[AppStore](https://apps.apple.com/us/app/pocketcas-mathematics-toolkit/id703006457)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Clipboard managers
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[iPaste](https://apps.apple.com/app/ipaste-clipboard-tool/id1056935452)| Simple menubar clipboard tool|[AppStore](https://apps.apple.com/app/ipaste-clipboard-tool/id1056935452)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Pasteapp](https://pasteapp.io/)| Powerful and easy to use clipboard manager for MacOS|[AppStore](https://apps.apple.com/us/app/paste-clipboard-history-manager/id967805235)|![Subscription](symbols/subscription.svg "Subscription")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Pastebot](https://tapbots.com/pastebot/)| Clipboard manager that works like a note app with filters and quick paste.|[AppStore](https://apps.apple.com/us/app/pastebot/id1179623856?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Pasta](https://getpasta.com/)| The most flexible clipboard manager for the mac|[AppStore](https://apps.apple.com/app/id1438389787)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Clipy](https://github.com/Clipy/Clipy)| Clipboard extension app for macOS.|[Github](https://github.com/Clipy/Clipy)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[Clipboard Center](http://www.kodlian.com/apps/clipboard-center)| Easy-to-use and powerful clipboard manager. Through a history, lists and a flexible UI, this app allows you to copy and paste texts, images, and files in the most efficient way.|[AppStore](https://apps.apple.com/us/app/clipboard-center/id599614786?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[CopyClip](https://apps.apple.com/us/app/copyclip-clipboard-history/id595191960?mt=12)| Very simple and efficient clipboard manager for your Mac. Running discreetly from your menu bar.|[AppStore](https://apps.apple.com/us/app/copyclip-clipboard-history/id595191960?mt=12)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[CopyClip 2](https://fiplab.com/apps/copyclip-for-mac)| A newer clipboard manager for your Mac running discreetly from your menu bar and features a search bar similar to spotlight and is integraded with the touchbar.|[AppStore](https://apps.apple.com/us/app/copyclip-2-clipboard-manager/id1020812363?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Unclutter](https://unclutterapp.com/)| A new handy place on your desktop for storing notes, files and pasteboard clips.|[AppStore](https://apps.apple.com/app/unclutter/id577085396)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[iClip – Clipboard Manager](http://iclipapp.com/)|Keep a history of almost anything you copy, instead of only the last thing.|[AppStore](https://apps.apple.com/app/id468369783?ls=1&mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Maccy](https://maccy.app/)| Clipboard manager for macOS which does one job - keep your copy history at hand. Period.|[Website](https://maccy.app/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|


## Contacts
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[busyContacts](https://www.busymac.com/busycontacts/)| Contact manager for macOS that makes creating, finding, and managing contacts faster and more efficient.|[AppStore](https://apps.apple.com/us/app/busycontacts/id964258399?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## CRM
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Daylite](https://www.marketcircle.com/)| Organize your clients, leads, projects all in one place so your team can share everything.|[Website](https://www.marketcircle.com/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Education / Science
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[EnzymeX](https://nucleobytes.com/enzymex/index.html)| The DNA sequence editor for OS X|[Website](https://nucleobytes.com/enzymex/index.html)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Findings](https://findingsapp.com/)| Your research assistant & lab notebook, all in one app.|[Website](https://findingsapp.com/)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Studies](http://www.studiesapp.com/)| Studies is a flashcard app for the serious student, with editions for Mac®, iPhone® and iPad®.|[AppStore](https://apps.apple.com/us/app/studies/id1071676469)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|



## File managers
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[ForkLift](https://binarynights.com/)| Most advanced dual panel file manager and transfer client for MacOS.|[Website](https://binarynights.com/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[PathFinder](https://cocoatech.com/#/)| Finder manager for MacOS since 2001|[Website](https://cocoatech.com/#/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[XtraFinder](https://www.trankynam.com/xtrafinder/)| Tabs, Dual Panel, and numerous features for Mac |[Website](https://www.trankynam.com/xtrafinder/)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Commander One](https://mac.eltima.com/file-manager.html)| Free dual pane file manager for Mac|[Website](https://mac.eltima.com/file-manager.html)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Graph / Diagram
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[OmniGraffle](https://www.omnigroup.com/omnigraffle)| Design diagram and rapid-prototypes.|[AppStore](https://apps.apple.com/us/app/omnigraffle-7/id1142578753?mt=12)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Diagrams](https://diagrams.app/)| The new diagram editor for Mac. Create beautiful, structured diagrams. As fast as you think.|[AppStore](https://apps.apple.com/app/apple-store/id1276248849?mt=8)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[SheetPlanner](https://sheetplanner.com/)| Organize information, plan your life and schedule your tasks and activities.|[AppStore](https://apps.apple.com/us/app/sheetplanner/id1438761088?mt=12)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[MindNode](https://mindnode.com/)| Most productive mindnote software on the Mac.|[AppStore](https://apps.apple.com/app/apple-store/id1289197285?mt=8)|![Subscription](symbols/subscription.svg "Subscription")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[4Peaks](https://nucleobytes.com/4peaks/index.html)| View and edit sequence trace files|[Website](https://nucleobytes.com/4peaks/index.html)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Mail
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Canary Mail](https://canarymail.io/)| Best email app on the Mac with native UI and end-to-end encryption.|[AppStore](https://apps.apple.com/us/app/canary-mail/id1236045954)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[AirMail](https://airmailapp.com/)| Airmail is a mail client designed with performance and intuitive interaction in mind optimized for macOS and iOS.|[AppStore](https://apps.apple.com/app/apple-store/id918858936)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Spark](https://sparkmailapp.com/)| Email with intelligent email prioritization, noise reduction, and advanced email tools.|[AppStore](https://apps.apple.com/us/app/spark-email-app-by-readdle/id1176895641?mt=12)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Markdown editors
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Typora](https://www.typora.io/)| Minimalistic WYSIWYG Markdown editor **☢ ELECTRON ☢**.|[Website](https://www.typora.io/)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[MacDown](https://macdown.uranusjr.com/)| Open source Markdown editor for macOS influenced by Mou.|[Website](https://macdown.uranusjr.com/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[Byword](https://bywordapp.com/)| Markdown app for writing in plain text efficiently cross devices.|[AppStore](https://apps.apple.com/app/byword/id420212497?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[LightPaper](https://getlightpaper.com/)| Simple, beautiful yet powerful text editor for your Mac|[Website](https://getlightpaper.com/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Drafts](https://getdrafts.com/)| Drafts, where text starts. Quickly capture text and send it almost anywhere!|[AppStore](https://apps.apple.com/app/id1435957248?mt=12)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Strike](https://strike-app.com/)| Modern writing app with powerful features like Smart Markdown, tables, attachments and photos, text sorting, tags, and more.|[AppStore](https://apps.apple.com/app/id1439733892)|![Subscription](symbols/subscription.svg "Subscription")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[QuickDraft](https://quickdraft.app/)| Simple scratchpad in your menubar|[AppStore](https://apps.apple.com/us/app/quick-draft-simple-scratchpad/id1496067471)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[iA Writer](https://ia.net/writer)| Clean, simple and distraction-free writing environment for when you really need to focus on your words.|[AppStore](https://apps.apple.com/us/app/ia-writer/id775737590?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[MWeb](https://www.mweb.im/)| Native pro markdown writing, note taking and static blog generator app.|[AppStore](https://apps.apple.com/us/app/mweb/id1403919533?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[MarkupTable](https://www.xelaton.com/en/Applications/MarkupTable)| The table generator for Markup-Formatted tables. Great companion for any writer who uses Markup-Languages to structure its texts.|[AppStore](https://apps.apple.com/us/app/markuptable/id1355702304?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Office
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Numbers](https://www.apple.com/numbers/)| Create impressive spreadsheets|[AppStore](https://apps.apple.com/us/app/numbers/id409203825?mt=12)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Keynote](https://www.apple.com/keynote/)| Build stunning presentations|[AppStore](https://apps.apple.com/us/app/keynote/id409183694?mt=12)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Pages](https://www.apple.com/pages/)| Documents that stand apart|[AppStore](https://apps.apple.com/us/app/pages/id409201541?mt=12)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Receipts](https://www.receipts-app.com/)| Receipts is ideal for collecting and managing receipts for tax purposes, for expenses, for preparation of bookkeeping and also for private households.|[AppStore](https://apps.apple.com/us/app/receipts/id1079833326?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## PDF
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[PDF Expert](https://pdfexpert.com/)| Fast, robust and beautiful PDF Editor for MacOS|[AppStore](https://apps.apple.com/us/app/pdf-expert-edit-and-sign-pdf/id1055273043?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[PDF Search](http://www.pdfsearchapp.com/)| Search multiple PDFs insanely fast|[AppStore](https://apps.apple.com/us/app/pdf-search/id1229271394?ls=1&mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[PDF Squeezer](https://witt-software.com/pdfsqueezer/)| Reduce the size of PDF files.|[AppStore](https://apps.apple.com/app/id1502111349)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[PDFCombo](https://www.onekerato.com/pdfcombo.html)| Simple utility to combine PDF documents - with special consideration for the table of contents (TOC) in the PDFs.|[AppStore](https://apps.apple.com/us/app/pdfcombo/id1030461463?mt=12)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[PDFExplode](https://www.onekerato.com/pdfexplode.html)| Explode your large PDF into many PDFs. Extract pages corresponding to any Table of Contents (TOC) entry in your PDF.|[AppStore](https://apps.apple.com/us/app/pdfexplode/id483327629?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[PDFpenPro](https://smilesoftware.com/pdfpenpro/)| Powerful PDF Editing On Your Mac|[AppStore](https://apps.apple.com/us/app/pdfpenpro-12/id1507044690?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Time management / tracking
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Stand](https://getstandapp.com/)| Just a simple app for your Mac that reminds you to stand up once an hour.|[Website](https://getstandapp.com/)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[JustFocus](http://climstudio.com/justfocus/)| Focus on what's important. Clean and simple pomodoro app.|[AppStore](https://apps.apple.com/us/app/just-focus/id1142151959?ls=1&mt=12)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[FreelanceStation](https://ds9soft.com/FreelanceStation/)| Time tracking and Invoicing for Mac. Built for freelancers. |[AppStore](https://apps.apple.com/us/app/freelancestation/id1461329806)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Tracktiq](https://tracktiq.com/)| Easy time tracking on MacOS.|[AppStore](https://apps.apple.com/se/app/tracktiq/id1165790525?mt=12)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Timing](https://timingapp.com/)| Just continue focusing on your work while Timing records your time automatically. |[Website](https://timingapp.com/)|![Subscription](symbols/subscription.svg "Subscription")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Timemator](https://timemator.com)| Automate your time-tracking and forget about the timer.|[Website](https://timemator.com)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Todo / GTD / Note-taking
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[FSNotes](https://fsnot.es/)| Notes on steroids for macOS and iOS. Can be used with plain/text, markdown or rtf.|[AppStore](https://apps.apple.com/app/fsnotes/id1277179284)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[OmniOutliner](https://www.omnigroup.com/omnioutliner/)| Capture and organize big ideas|[AppStore](https://apps.apple.com/us/app/omnioutliner-5/id1142578772?mt=12)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[OmniFocus](https://www.omnigroup.com/omnifocus)| OmniFocus is powerful task management software for busy professionals.|[AppStore](https://apps.apple.com/us/app/omnifocus-3/id1346203938?mt=12)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Agenda](https://agenda.com/)| Elegant date-focused note taking|[AppStore](https://apps.apple.com/us/app/agenda/id1287445660?mt=12)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Things](https://culturedcode.com/things/)| Personal task manager that helps you achieve your goals. |[AppStore](https://apps.apple.com/us/app/things-3/id904280696?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[PopDo](https://ds9soft.com/PopDo/)| A quick task list for your Mac menu bar|[AppStore](https://apps.apple.com/us/app/popdo/id1493198199)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Firetask](https://firetask.com/)| Native cross-devices GTD with classical task management features such as due dates and priorities in a unique way.|[AppStore](https://apps.apple.com/us/app/firetask-pro/id1259892758?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Gooba](https://goobapp.com/)| Writing app and task manager in one with simple and interactive design|[AppStore](https://apps.apple.com/it/app/gooba/id1484262241?mt=12)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[NotePlan](https://noteplan.co/)| Manage your calendar, todos and notes in one place.|[AppStore](https://apps.apple.com/us/app/noteplan-2/id1137020764?mt=12)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[GoodTask](https://goodtaskapp.com/)| Powerful Task/Project Manager based on Apple's Reminders & Calendars|[AppStore](https://apps.apple.com/app/id1143437985)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Noto](http://noto.ink/)| Modern writing app with delightful interactions, powerful editing tools, and a beautiful design.|[AppStore](https://apps.apple.com/app/noto-elegant-note/id1459055246)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[GoodNotes](https://www.goodnotes.com/)| Transform your Mac into smart digital paper and a powerful document management system. Note-Taking & PDF Markup|[AppStore](https://apps.apple.com/us/app/goodnotes-5/id1444383602?ls=1)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[KeepIt](https://reinventedsoftware.com/keepit/)| Keep It is notebook, scrapbook and organizer, ideal for writing notes, saving web links, storing documents, images or any kind of file, and finding them again.|[AppStore](https://apps.apple.com/us/app/keep-it/id1272768911?mt=12)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[SideNotes](https://www.apptorium.com/sidenotes)| Quick notes on the side of your screen.|[AppStore](https://apps.apple.com/us/app/sidenotes/id1441958036?ls=1&mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[TickTick](https://ticktick.com/)| Cross-platform todo list, checklist and task manager app.|[AppStore](https://apps.apple.com/app/id966085870)|![Subscription](symbols/subscription.svg "Subscription")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Tools
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Alfred](https://www.alfredapp.com/)| Search your Mac and the web, and be more productive with custom actions to control your Mac.|[Website](https://www.alfredapp.com/)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Yoink](https://eternalstorms.at/yoink/mac/)| Simplify and improve drag and drop on your Mac and speed up your daily workflow. |[AppStore](https://apps.apple.com/app/yoink/id457622435?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Dropover](https://dropoverapp.com/?r=as_en)| Make Drag & Drop easier. Use it to stash, gather or move any draggable content without having to open side-by-side windows. |[AppStore](https://apps.apple.com/us/app/dropover/id1355679052?mt=12)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Keyboard Mestro](http://www.keyboardmaestro.com/main/)| Automate applications or web sites, text or images, simple or complex, on command or scheduled. You can automate virtually anything.|[Website](http://www.keyboardmaestro.com/main/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[LaunchBar](https://www.obdev.at/products/launchbar/index.html)| Start with a single keyboard shortcut to access and control every aspect of your digital life.|[Website](https://www.obdev.at/products/launchbar/index.html)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[PopClip](https://pilotmoon.com/popclip/)| Get the iOS-like context menus for text selection|[AppStore](https://apps.apple.com/us/app/popclip/id445189367?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[CheatSheet](https://www.cheatsheetapp.com/CheatSheet/)| Hold the ⌘-Key to show a list of all active shortcuts of the current application. |[Website](https://www.cheatsheetapp.com/CheatSheet/)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[BetterRename](https://www.publicspace.net/BetterRename/)| Advanced batch renamer for MacOS.|[AppStore](https://apps.apple.com/us/app/better-rename-11/id1501308038?ls=1)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Flour](https://fluorapp.net/)| A handy tool for macOS allowing you to switch Fn keys' mode based on active application. |[Website](https://fluorapp.net/)|![Free](symbols/free.svg "Free")|![Open-Source](symbols/open.svg "Open-Source")|
|[Gemini](https://macpaw.com/gemini)| The intelligent duplicate file finder|[AppStore](https://apps.apple.com/app/gemini-2-the-duplicate-finder/id1090488118?mt=12)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Keystroke Pro](https://ixeau.com/keystroke-pro/)| Keypress Visualizer|[Website](https://ixeau.com/keystroke-pro/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Hazel](https://www.noodlesoft.com/)| Automated Organization for Your Mac. Hazel watches whatever folders you tell it to, automatically organizing your files according to the rules you create.|[Website](https://www.noodlesoft.com/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Keysmith](http://www.keysmith.app/)| Create custom keyboard shortcuts for your Mac and the web. |[Website](http://www.keysmith.app/)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Writing
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[iBooks Author](https://www.apple.com/ibooks-author/)| Create your own interactive ebook.|[AppStore](https://apps.apple.com/us/app/ibooks-author/id490152466?ls=1&mt=12)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[SmartCapsLock](https://kishanbagaria.com/smartcapslock/)| Don't you get annoyed when you accidentally type everything with caps lock on.|[Website](https://kishanbagaria.com/smartcapslock/)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Storyist](https://www.storyist.com/)| A powerful writing environment for novelists and screenwriters|[AppStore](https://apps.apple.com/us/app/storyist-4/id1445242832?mt=12)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Ulysses](https://ulysses.app/)| The Ultimate Writing App for Mac, iPad and iPhone|[AppStore](https://apps.apple.com/app/ulysses/id1225570693?l=en&mt=12)|![Subscription](symbols/subscription.svg "Subscription")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Bear](https://bear.app/)| Bear is a beautiful, flexible writing app for crafting notes and prose.|[AppStore](https://apps.apple.com/us/app/bear-beautiful-writing-app/id1091189122?ls=1&mt=12)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Scrivener](https://www.literatureandlatte.com/scrivener/overview)| The go-to app for writers of all kinds, used every day by best-selling novelists, screenwriters, students, academics, journalists etc. |[AppStore](https://apps.apple.com/us/app/scrivener-3/id1310686187?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Highland 2](https://quoteunquoteapps.com/highland-2/)| We’ve taken the tools we built for writing screenplays and made them work for almost every kind of document you write with clean design and innovative tools.|[AppStore](https://apps.apple.com/se/app/highland-2/id1171820258?mt=12)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Slugline](https://slugline.co/)| The best-reviewed screenwriting app for Mac.|[AppStore](https://apps.apple.com/us/app/slugline-2/id1483507254?mt=12)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Texpad](https://www.texpad.com/)| Join academics and professional writers in harnessing the power of LaTeX to produce polished output of your writing.|[Website](https://www.texpad.com/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Manuscripts](https://www.manuscriptsapp.com/)| Your next manuscript, from A to Z.|[Website](https://www.manuscriptsapp.com/)|![Free](symbols/free.svg "Free")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Writing tools
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[Rocket Typist](https://witt-software.com/rockettypist/)| Boost your typing with premade snippets.|[Website](https://witt-software.com/rockettypist/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[TextExpander](https://textexpander.com/)| Easily insert text snippets in any application from a library of content created by you and your team. |[Website](https://textexpander.com/)|![Subscription](symbols/subscription.svg "Subscription")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Rocket](https://matthewpalmer.net/rocket/)| Mind-blowing emoji on your Mac. Like Slack and Discord.|[Website](https://matthewpalmer.net/rocket/)|![Freemium](symbols/freemium.svg "Freemium")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Typinator](https://www.ergonis.com/products/typinator/)| Fastest text-expander for the Mac.|[Website](https://www.ergonis.com/products/typinator/)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[TypeIt4Me](https://www.ettoresoftware.com/)| The original text-expander for the Mac. Save time and effort. Type more with fewer keystrokes.|[AppStore](https://apps.apple.com/se/app/typeit4me/id412141729?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|


## Other
| Name ⇵ | Description ⇵ | Get it ⇵ | ⇵ | ⇵ |
|:-------|:--------------|:--------:|:-:|:-:|
|[start](https://apps.apple.com/us/app/start/id1329701389)| Get a windows start menu for your mac.|[AppStore](https://apps.apple.com/us/app/start/id1329701389)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[Paprika](https://www.paprikaapp.com/)| Helps you organize your recipes, make meal plans, and create grocery lists. |[AppStore](https://apps.apple.com/us/app/paprika-recipe-manager-3/id1303222628?ls=1&mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
|[SwitchGlass](https://apps.apple.com/us/app/switchglass/id1498546559?mt=12)| SwitchGlass adds a dedicated application switcher to your Mac.|[AppStore](https://apps.apple.com/us/app/switchglass/id1498546559?mt=12)|![Paid](symbols/paid.svg "Paid")|![Closed-Source](symbols/closed.svg "Closed-Source")|
